﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ECommerce.Models
{
    public class City
    {
        [Key]
        public int CityId { get; set; }

        [Required(ErrorMessage = "the fiel {0} is required")]
        [MaxLength(50, ErrorMessage = "the fiel {0} must be maximun {1} characters length")]
        [Display(Name = "ciudad")]

        public string Name { get; set; }

        [Required(ErrorMessage = "the fiel {0} is required")]
        [Range (1, double.MaxValue, ErrorMessage = "debe seleccionar un {0}")]
        [Display(Name = "Departamento")]

        public int DepartmentId { get; set; }

        public virtual Department Department { get; set; }

        public virtual ICollection<Company> Companies { get; set; }


    }
}