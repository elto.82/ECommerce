﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ECommerce.Models
{
    public class Company
    {
        [Key]
        public int CompanyId { get; set; }

        [Required(ErrorMessage = "the fiel {0} is required")]
        [MaxLength(50, ErrorMessage = "the fiel {0} must be maximun {1} characters length")]
        [Display(Name = "Compañia")]
        [Index("Company_Name_Index", IsUnique = true)]
        public string Name { get; set; }

        [Required(ErrorMessage = "the fiel {0} is required")]
        [MaxLength(50, ErrorMessage = "the fiel {0} must be maximun {1} characters length")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Required(ErrorMessage = "the fiel {0} is required")]
        [MaxLength(100, ErrorMessage = "the fiel {0} must be maximun {1} characters length")]
        public string Address { get; set; }

        [DataType(DataType.ImageUrl)]
        public string Logo { get; set; }

        [Required(ErrorMessage = "the fiel {0} is required")]
        [Range(1, double.MaxValue, ErrorMessage = "debe seleccionar un {0}")]
        [Display(Name = "Departamento")]
        public int DepartmentId { get; set; }

        [Required(ErrorMessage = "the fiel {0} is required")]
        [Range(1, double.MaxValue, ErrorMessage = "debe seleccionar un {0}")]
        [Display(Name = "ciudad")]
        public int CityId { get; set; }

        [NotMapped]
        public HttpPostedFileBase LogoFile { get; set; }

        public virtual Department Department { get; set; }

        public virtual City City { get; set; }

    }

}